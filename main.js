// @ts-check

const sdk = require("node-appwrite");
const cfg = require("./config");
const fs = require("fs");
const { Readable } = require("stream");

const appwrite = new sdk.Client()
  .setEndpoint(cfg.APPWRITE_ENDPOINT)
  .setProject(cfg.APPWRITE_PROJECT_ID)
  .setKey(cfg.APPWRITE_API_KEY);

const storage = new sdk.Storage(appwrite);

async function main() {
  try {
    if (!(await storage.getBucket("test"))) throw new Error();
  } catch {
    await storage.createBucket("test", "test");
  }

  const file = fs.readFileSync("./test.png");
  await storage.createFile(
    "test",
    "test.png",
    new sdk.InputFile(Readable.from(file), "test.png", Buffer.byteLength(file))
  );
}

main();
