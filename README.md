# Appwrite Inputfile Demo

Demo for https://github.com/appwrite/sdk-generator/pull/583

Open this project in GitPod, and it should automatically setup Appwrite for you.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/arnu515-projects/temp/appwrite-inputfile-demo)
